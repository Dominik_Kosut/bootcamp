DROP TABLE IF EXISTS car CASCADE;
DROP TABLE IF EXISTS detail CASCADE;

--CREATE TABLE Car(
--id INT AUTO_INCREMENT NOT NULL,
--owner VARCHAR(255) DEFAULT NULL,
--car_type VARCHAR(255) DEFAULT NULL,
--fuel_type VARCHAR(255) DEFAULT NULL,
--PRIMARY KEY (id)
--);
--
--CREATE TABLE Detail(
--id INT AUTO_INCREMENT NOT NULL,
--info VARCHAR(255) DEFAULT NULL,
--PRIMARY KEY (id)
--);
--
--insert into Owner values(1, 11, 'domkos@gmail.com', 'Dominik', 'Košut');
--insert into Owner values(1, 22, 'pavelk@gmail.com', 'Pavel', 'Kalousek');
--insert into Owner values(1, 33, 'antoninek.maly@gmail.com', 'Antonín', 'Postrčil');
--
--
--insert into Car values(1, 'Žlutá', 'Škoda - Fabia', 'Benzín');
--insert into Car values(2, 'Bílá Šímek', 'BMW', 'Nafta');
--insert into Car values(3, 'Černá', 'Citroen C3', 'Plyn');
--
--insert into Detail values(1, 111, 'Jezdí to dobře :)', 1, 11, null);
--insert into Detail values(1, 222, 'Jezdí to špatně :(', 2, 22, null);