package com.gasoline.GasolineApp.service;

import com.gasoline.GasolineApp.model.CarVO;

import java.util.List;

public interface CarService {

    List<CarVO> findAll();

    CarVO findById(int id);

    void save(CarVO car);

    void update(CarVO carVO, int id);

    void deleteById(int id);

    List<CarVO> getAllOwnersCars(int id);

    CarVO addCarToOwner(CarVO carVO, int ownID);

    void setCarToOwner(int carID, int ownID);
}
