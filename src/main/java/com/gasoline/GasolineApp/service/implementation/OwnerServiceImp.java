package com.gasoline.GasolineApp.service.implementation;

import com.gasoline.GasolineApp.entity.Owner;
import com.gasoline.GasolineApp.model.OwnerVO;
import com.gasoline.GasolineApp.repository.OwnerRepository;
import com.gasoline.GasolineApp.service.OwnerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OwnerServiceImp implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public List<OwnerVO> findAll() {
        return ownerRepository
                .findAll()
                .stream()
                .map(element -> modelMapper.map(element, OwnerVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public OwnerVO findById(int id) {
        Optional<Owner> result = ownerRepository.findById(id);
        Owner owner = null;
        if (result.isPresent()) {
            owner = result.get();
        } else {
            throw new RuntimeException("Owner with ID: " + id + " does not exist!");
        }
        return modelMapper.map(owner, OwnerVO.class);
    }

    @Override
    public void save(OwnerVO ownerVO) {
        Owner owner = modelMapper.map(ownerVO, Owner.class);
        owner.setId(0);
        ownerRepository.save(owner);
    }

    @Override
    public void update(OwnerVO ownerVO, int id) {
        Optional<Owner> result = ownerRepository.findById(id);
        Owner owner = null;
        if (result.isPresent()) {
            owner = result.get();
        } else {
            throw new RuntimeException("Owner with ID: " + id + " does not exist!");
        }
        modelMapper.map(ownerVO, owner);
        ownerRepository.save(owner);
    }

    @Override
    public void deleteById(int id) {
        ownerRepository.deleteById(id);
    }
}
