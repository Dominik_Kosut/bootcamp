package com.gasoline.GasolineApp.service.implementation;

import com.gasoline.GasolineApp.entity.Car;
import com.gasoline.GasolineApp.model.CarVO;
import com.gasoline.GasolineApp.repository.CarRepository;
import com.gasoline.GasolineApp.repository.OwnerRepository;
import com.gasoline.GasolineApp.service.CarService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarServiceImp implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<CarVO> findAll() {
        return carRepository
                .findAll()
                .stream()
                .map(element -> modelMapper.map(element, CarVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public CarVO findById(int id) {
        Optional<Car> result = carRepository.findById(id);
        Car car = null;
        if(result.isPresent()){
            car = result.get();
        }else{
            throw new RuntimeException("Car with ID: " + id + " does not exist!");
        }
        return modelMapper.map(car, CarVO.class);
    }

    @Override
    public void save(CarVO carVO) {
        Car car = modelMapper.map(carVO, Car.class);
        car.setId(0);
        carRepository.save(car);
    }

    @Override
    public void update(CarVO carVO, int id) {
        Optional<Car> result = carRepository.findById(id);
        Car car = null;
        if (result.isPresent()) {
            car = result.get();
        } else {
            throw new RuntimeException("Car with ID: " + id + " does not exist!");
        }
        modelMapper.map(carVO, car);
        carRepository.save(car);
    }

    @Override
    public void deleteById(int id) {
        carRepository.deleteById(id);
    }

    @Override
    public List<CarVO> getAllOwnersCars(int id) {
        return carRepository.findAllByOwnersId(id)
                .stream()
                .map(element -> modelMapper.map(element, CarVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public CarVO addCarToOwner(CarVO carVO, int ownID) {
        Car car = modelMapper.map(carVO, Car.class);
        car.setId(0);
        car.addOwner(ownerRepository.getById(ownID));
        carRepository.save(car);
        return carVO;
    }

    @Override
    public void setCarToOwner(int carID, int ownID) {
        Car car = carRepository.getById(carID);
        car.addOwner(ownerRepository.getById(ownID));
        carRepository.save(car);
    }

}
