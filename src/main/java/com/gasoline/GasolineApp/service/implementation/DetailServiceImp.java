package com.gasoline.GasolineApp.service.implementation;

import com.gasoline.GasolineApp.entity.Car;
import com.gasoline.GasolineApp.entity.Detail;
import com.gasoline.GasolineApp.model.DetailVO;
import com.gasoline.GasolineApp.repository.CarRepository;
import com.gasoline.GasolineApp.repository.DetailRepository;
import com.gasoline.GasolineApp.service.DetailService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DetailServiceImp implements DetailService {

    @Autowired
    private DetailRepository detailRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<DetailVO> findAll() {
        return detailRepository
                .findAll()
                .stream()
                .map(element -> modelMapper.map(element, DetailVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public DetailVO findById(int id) {
        Optional<Detail> result = detailRepository.findById(id);
        Detail detail;

        if (result.isPresent()) {
            detail = result.get();
        } else {
            throw new RuntimeException("Detail with ID: " + id + " does not exist!");
        }
        return modelMapper.map(detail, DetailVO.class);
    }

    @Override
    public void save(DetailVO detailVO) {
        Detail detail = modelMapper.map(detailVO, Detail.class);
        detail.setId(0);
        detailRepository.save(detail);
    }

//    @Override
//    public void update(DetailVO detailVO, int carID, int detID) {
//        Detail detailDB = detailRepository.getById(detID);
//        modelMapper.map(detailVO, detailDB);
//        detailRepository.save(detailDB);
//    }

    @Override
    public void update(int id, DetailVO detailVO){
        Optional<Detail> result = detailRepository.findById(id);
        Detail detail;
        if (result.isPresent()) {
            detail = result.get();
        } else {
            throw new RuntimeException("Car with ID: " + id + " does not exist!");
        }
        modelMapper.map(detailVO, detail);
        detailRepository.save(detail);
    }

    @Override
    public void deleteById(int id) {
        detailRepository.deleteById(id);
    }

    @Override
    public List<DetailVO> getAllCarDetails(int carID) {
        Car car = carRepository.getById(carID);
        List<Detail> details = car.getDetails();
        return details
                .stream()
                .map(element -> modelMapper.map(element, DetailVO.class))
                .collect(Collectors.toList());
    }

    @Override
    public DetailVO addDetailToCar(DetailVO detailVO, int carID) {
        Car carDB = carRepository.getById(carID);
        Detail detail = modelMapper.map(detailVO, Detail.class);
        detail.setId(0);
        detail.setCar(carDB);
        detailRepository.save(detail);
        return detailVO;
    }

}
