package com.gasoline.GasolineApp.service;

import com.gasoline.GasolineApp.model.DetailVO;

import java.util.List;

public interface DetailService {

    List<DetailVO> findAll();

    DetailVO findById(int id);

    void save(DetailVO detailVO);

//    void update(DetailVO detailVO, int carID, int detID);

    void update(int detID, DetailVO detailVO);

    void deleteById(int id);

    List<DetailVO> getAllCarDetails(int carID);

    DetailVO addDetailToCar(DetailVO detailVO, int carID);
}
