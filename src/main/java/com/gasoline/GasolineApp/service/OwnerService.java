package com.gasoline.GasolineApp.service;

import com.gasoline.GasolineApp.model.OwnerVO;

import java.util.List;

public interface OwnerService {

    List<OwnerVO> findAll();

    OwnerVO findById(int id);

    void save(OwnerVO ownerVO);

    void update(OwnerVO ownerVO, int id);

    void deleteById(int id);
}
