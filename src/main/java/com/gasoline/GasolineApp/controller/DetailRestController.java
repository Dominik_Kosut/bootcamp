package com.gasoline.GasolineApp.controller;

import com.gasoline.GasolineApp.model.DetailVO;
import com.gasoline.GasolineApp.service.CarService;
import com.gasoline.GasolineApp.service.DetailService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DetailRestController {

    @Autowired
    private DetailService detailService;
    @Autowired
    private CarService carService;

    @GetMapping("/details")
    public List<DetailVO> findAllDetails() {
        return detailService.findAll();
    }

    @GetMapping("/details/{detailId}")
    public DetailVO getDetailById(@PathVariable("detailId") int detailId) {
        return detailService.findById(detailId);
    }

    @PutMapping("/details/{id}")
    public DetailVO editDetail(@PathVariable("id") int id, @RequestBody DetailVO detailVO){
        detailService.update(id, detailVO);
        return detailVO;
    }

    @DeleteMapping("/details/{detailId}")
    public boolean deleteInfo(@PathVariable("detailId") int detID) {
        DetailVO detailVOdB = detailService.findById(detID);
        if (detailVOdB == null) {
            throw new RuntimeException("Detail with ID: " + detID + " not found!");
        }
        detailService.deleteById(detID);
        return true;
    }

    @Operation(summary = "Vypíše všechny detaily konkrétního auta")
    @GetMapping("/cars/{carId}/details")
    public List<DetailVO> findAllCarDetails(@PathVariable("carId") int carId) {
        return detailService.getAllCarDetails(carId);
    }

//    @Operation(summary = "Upraví detail u auta")
//    @PutMapping("/cars/{carId}/details/{detId}")
//    public DetailVO editCarDetail(
//            @Parameter(description = "Car ID") @PathVariable("carId") int carId,
//            @Parameter(description = "Detail ID") @PathVariable("detId") int detId, @RequestBody DetailVO detailVO) {
//
//        detailService.update(detailVO, carId, detId);
//        return detailVO;
//    }

    @Operation(summary = "Přidá nový detail k autu")
    @PostMapping("/cars/{carId}/details")
    public DetailVO createCarDetail(@PathVariable("carId") int carId, @RequestBody DetailVO detailVO) {
        return detailService.addDetailToCar(detailVO, carId);
    }
}
