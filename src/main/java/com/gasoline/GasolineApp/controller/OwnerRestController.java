package com.gasoline.GasolineApp.controller;

import com.gasoline.GasolineApp.model.OwnerVO;
import com.gasoline.GasolineApp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OwnerRestController {

    @Autowired
    private OwnerService ownerService;

    @GetMapping("/owners")
    public List<OwnerVO> findAllOwners() {
        return ownerService.findAll();
    }

    @GetMapping("/owners/{ownerID}")
    public OwnerVO getOwnerById(@PathVariable("ownerID") int id) {
        OwnerVO ownerVO = ownerService.findById(id);
        if (ownerVO == null) {
            throw new RuntimeException("Owner with ID: " + id + " not found!");
        }
        return ownerVO;
    }

    @PostMapping("/owners")
    public OwnerVO addOwner(@RequestBody OwnerVO newOwner) {
        ownerService.save(newOwner);
        return newOwner;
    }

    @PutMapping("/owners/{ownerID}")
    public OwnerVO updateOwner(@PathVariable("ownerID") int id, @RequestBody OwnerVO editOwner) {
        ownerService.update(editOwner, id);
        return editOwner;
    }

    @DeleteMapping("/owners/{ownerID}")
    public boolean deleteOwner(@PathVariable("ownerID") int id) {
        OwnerVO ownerVOdB = ownerService.findById(id);
        if (ownerVOdB == null) {
            throw new RuntimeException("Owner with ID: " + id + " not found!");
        }
        ownerService.deleteById(id);
        return true;
    }
}
