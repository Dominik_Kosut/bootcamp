package com.gasoline.GasolineApp.controller;

import com.gasoline.GasolineApp.model.CarVO;
import com.gasoline.GasolineApp.service.CarService;
import com.gasoline.GasolineApp.service.OwnerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarRestController {

    @Autowired
    private OwnerService ownerService;
    @Autowired
    private CarService carService;

    @GetMapping("/cars")
    public List<CarVO> findAllCars() {
        return carService.findAll();
    }

    @GetMapping("/cars/{carId}")
    public CarVO getCarById(@PathVariable("carId") int carId) {
        CarVO carVO = carService.findById(carId);
        if (carVO == null) {
            throw new RuntimeException("Car with ID: " + carId + " not found!");
        }
        return carVO;
    }

    @PostMapping("/cars")
    public CarVO addCar(@RequestBody CarVO newCarVO) {
        carService.save(newCarVO);
        return newCarVO;
    }

    @PutMapping("/cars/{carId}")
    public CarVO updateCar(@PathVariable("carId") int carId, @RequestBody CarVO editCarVO) {
        carService.update(editCarVO, carId);
        return editCarVO;
    }

    @DeleteMapping("/cars/{carId}")
    public boolean deleteCar(@PathVariable("carId") int carId) {
        CarVO carVObB = carService.findById(carId);
        if (carVObB == null) {
            throw new RuntimeException("Car with ID: " + carId + " not found!");
        }
        carService.deleteById(carId);
        return true;
    }

    @Operation(summary = "Vrati všechna auta daného uživatele")
    @GetMapping("owners/{ownerId}/cars")
    public List<CarVO> getAllOwnersCars(@PathVariable("ownerId") int ownId) {
        return carService.getAllOwnersCars(ownId);
    }

    @Operation(summary = "Přidá nové auto k majiteli")
    @PostMapping("owners/{ownerId}/cars")
    public CarVO createOwnerCar(@PathVariable("ownerId") int ownId, @RequestBody CarVO carVO) {
        return carService.addCarToOwner(carVO, ownId);
    }

    @Operation(summary = "Přidá stávající auto k majiteli")
    @PutMapping("owners/{ownerId}/cars/{carId}")
    public String putOwnerCar(@PathVariable("ownerId") int ownId, @PathVariable("carId") int carId) {
        carService.setCarToOwner(carId, ownId);
        return "Updated";
    }
}
