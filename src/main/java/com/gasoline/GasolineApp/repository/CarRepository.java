package com.gasoline.GasolineApp.repository;

import com.gasoline.GasolineApp.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {
    List<Car> findAllByOwnersId(Integer id);
}
