package com.gasoline.GasolineApp.repository;

import com.gasoline.GasolineApp.entity.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OwnerRepository extends JpaRepository<Owner, Integer> {
}
