package com.gasoline.GasolineApp.repository;

import com.gasoline.GasolineApp.entity.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailRepository extends JpaRepository<Detail, Integer> {
}
