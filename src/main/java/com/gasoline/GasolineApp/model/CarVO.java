package com.gasoline.GasolineApp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CarVO {

    private Integer id;
    private String color;
    private String carType;
    private String fuelType;
}
