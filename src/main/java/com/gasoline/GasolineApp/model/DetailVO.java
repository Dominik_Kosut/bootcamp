package com.gasoline.GasolineApp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DetailVO {

    private Integer id;
    private String info;
    private double price;
    private double kilometers;
    private double amount;
}
