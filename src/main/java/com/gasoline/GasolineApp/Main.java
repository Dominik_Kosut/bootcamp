package com.gasoline.GasolineApp;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class Main {

	// Swagger -> Vojta doporučil springdoc
//	@Bean
//	public ModelMapper modelMapper() {
//		return new ModelMapper();
//	}

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

}
